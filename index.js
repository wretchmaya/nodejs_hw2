const express = require('express');
const app = express();
const { PORT } = require('./config/serverConfig');

const mongoose = require('mongoose');
const { DB_NAME, DB_PASSWORD, DB_USER_NAME } = require('./config/databaseConfig');
mongoose.connect(
	`mongodb+srv://${DB_USER_NAME}:${DB_PASSWORD}@cluster0.ocz36.mongodb.net/${DB_NAME}?retryWrites=true&w=majority`,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
		useCreateIndex: true,
	}
);

const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/user');
const noteRoutes = require('./routes/note');

app.use(express.json());

app.use('/api', authRoutes);
app.use('/api', userRoutes);
app.use('/api', noteRoutes);

app.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}`);
});
