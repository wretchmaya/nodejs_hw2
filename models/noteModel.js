const mongoose = require('mongoose');
module.exports = mongoose.model('Notes', {
	userId: {
		type: String,
		required: true,
	},
	completed: {
		type: Boolean,
		default: false,
	},
	text: {
		type: String,
		required: true,
	},
	createdDate: {
		type: String,
		default: new Date(),
	},
});

