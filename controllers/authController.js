const jwt = require('jsonwebtoken');
const { SECRET } = require('../config/authConfig');
const User = require('../models/userModel');

module.exports.register = (req, res) => {
	const { username, password } = req.body;
	if (!password) {
		return res.status(400).json({ message: 'Password is required' });
	}
	if (!username) {
		return res.status(400).json({ message: 'Username is required' });
	}
	const user = new User({
		username,
		password,
	});
	user
		.save()
		.then(() => {
			res.status(200).json({ message: 'Succsess' });
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};

module.exports.login = (req, res) => {
	const { username, password } = req.body;
	if (!password) {
		return res.status(400).json({ message: 'Password is required' });
	}
	if (!username) {
		return res.status(400).json({ message: 'Username is required' });
	}

	User.findOne({ username, password })
		.exec()
		.then(user => {
			if (!user) {
				return res.status(400).json({ message: 'User has not been found' });
			}
			res.status(200).json({
				message: 'Succsess',
				jwt_token: jwt.sign(JSON.stringify(user), SECRET),
			});
		});
};
