const User = require('../models/userModel');
module.exports.getInfo = (req, res) => {
	const userID = req.user._id;

	User.findById(userID)
		.exec()
		.then(user => {
			if (!user) {
				return res.status(400).json({ message: 'No such user has been found' });
			}

			res.json({
				user: {
					_id: user._id,
					username: user.username,
					createdDate: user.createdDate,
				},
			});
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};
module.exports.deleteUser = (req, res) => {
	const userID = req.user._id;

	if (!userID) {
		return res.status(400).json({ message: 'No such user has been found' });
	}
	User.findByIdAndDelete(userID)
		.exec()
		.then(() => {
			res.status(200).json({ message: 'Succsess' });
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};
