const Note = require('../models/noteModel');

module.exports.getNotes = (req, res) => {
	const userId = req.user._id;
	Note.find({ userId })
		.exec()
		.then(notes => {
			if (!notes) {
				return res.status(400).json({ message: 'No notes' });
			}
			res.status(200).json({ notes });
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};
module.exports.addNote = (req, res) => {
	const userId = req.user._id;
	const { text } = req.body;
	if (!text) {
		return res.status(401).json({ message: 'Text field is empty' });
	}
	const note = new Note({
		userId,
		text,
	});
	note
		.save()
		.then(() => {
			res.status(200).json({ message: 'Succsess' });
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};
module.exports.getNoteById = (req, res) => {
	const noteId = req.params.id;
	const userId = req.user._id;
	if (!noteId) {
		return res.status(400).json({ message: 'Incorrect parameters' });
	}
	Note.findOne({
		_id: noteId,
		userId,
	})
		.exec()
		.then(note => {
			if (!note) {
				return res.status(400).json({
					message: `No note with id ${noteId} has been found`,
				});
			}
			res.status(200).json({ note });
		});
};
module.exports.deleteNote = (req, res) => {
	const noteId = req.params.id;
	const userId = req.user._id;
	if (!noteId) {
		return res.status(400).json({ message: 'Incorrect parameters' });
	}
	Note.findOneAndDelete({
		_id: noteId,
		userId,
	})
		.exec()
		.then(note => {
			if (!note) {
				return res
					.status(400)
					.json({ message: `No note with id ${noteId} has been found` });
			}
			res.status(200).json({ message: 'Succsess' });
		});
};
module.exports.checkNoteStatus = (req, res) => {
	const noteId = req.params.id;
	const userId = req.user._id;
	if (!noteId) {
		return res.status(400).json({ message: 'Incorrect parameters' });
	}
	let noteStatus;
	Note.findOne({
		_id: noteId,
		userId,
	})
		.exec()
		.then(note => {
			if (!note) {
				return res.status(400).json({
					message: `No note with id ${noteId} has been found`,
				});
			}
			noteStatus = note.completed;
		})
		.then(() =>
			Note.findOneAndUpdate(
				{
					_id: noteId,
					userId,
				},
				{
					$set: {
						completed: !noteStatus,
					},
				}
			).exec()
		)
		.then(() => {
			res.status(200).json({ message: 'Success' });
		})
		.catch(err => {
			res.status(500).json({ message: err.message });
		});
};
