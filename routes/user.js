const express = require('express');
const router = express.Router();

const { getInfo, deleteUser } = require('../controllers/userController');
const authMiddleware = require('../middlewares/authMiddleware');

router.get('/users/me', authMiddleware, getInfo);
router.delete('/users/me', authMiddleware, deleteUser);

module.exports = router;
