const express = require('express');
const router = express.Router();

const {
	getNotes,
	addNote,
	getNoteById,
	deleteNote,
	checkNoteStatus
} = require('../controllers/noteController');
const authMiddleware = require('../middlewares/authMiddleware');

router.get('/notes', authMiddleware, getNotes);
router.get('/notes/:id', authMiddleware, getNoteById);
router.post('/notes', authMiddleware, addNote);
router.patch('/notes/:id', authMiddleware, checkNoteStatus)
router.delete('/notes/:id', authMiddleware, deleteNote);
module.exports = router;
